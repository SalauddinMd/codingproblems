package com.epam.Trees;

import java.util.ArrayList;
import java.util.Map;

public class VerticalOrder {
	
	public void vertical(Tree root,Map<Integer, ArrayList<Integer>> map,int dist)
	{
		if (root == null) {
			return;
		}
		ArrayList<Integer> list = map.get(dist);
		if (list == null) {
			list = new ArrayList<>();
			list.add(root.data);
		}
		else {
			list.add(root.data);
		}
		map.put(dist, list);
		vertical(root.left, map, dist-1);
		vertical(root.right, map, dist+1);
	}

	public static void main(String[] args) {
		
		

	}

}
