package com.epam.Trees;

import java.util.ArrayList;
import java.util.HashMap;

public class Diagonal {

	public void diagonal(Tree root,int d,HashMap<Integer, ArrayList<Integer>> map)
	{
		if (root == null) {
			return;
		}
		
		ArrayList<Integer> list = map.get(d);
		if (list == null) {
			list = new ArrayList<>();
		}
		list.add(root.data);
		map.put(d, list);
		diagonal(root.left, d+1, map);
		diagonal(root.right, d, map);
	}
	
	public static void main(String[] args) {
		

	}

}
