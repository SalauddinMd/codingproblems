public class AVLTree
{
	private class QueueNode
	{
		AVLTree treeNode;
		int level;

		public QueueNode(AVLTree treeNode,int level)
		{
			this.treeNode = treeNode;
			this.level = level;
		}
	}

	private class AVLTreeNode
	{
		int data;
		AVLTreeNode left;
		AVLTreeNode right;
		int height;

		public AVLTreeNode(int data)
		{
			this.data=data;
			this.height=1;
		}
	}

	AVLTreeNode root;
	public AVLTree()
	{
		this.root = new AVLTreeNode(9);
	}
	public AVLTree(int rootValue)
	{
		this.root = new AVLTreeNode(rootValue);
	}

	public int getHeight(AVLTreeNode node)
	{
		if (node == null) {
			return 0;
		}
		return node.height;
	}
	void updatedHeight(AVLTreeNode node)
	{
		if (node == null) {
			return;
		}
		node.height = Math.max(getHeight(node.left),getHeight(node.right)) + 1;
	}


	AVLTreeNode rotateRight(AVLTreeNode node)
	{
		if (node == null) {
			return node;
		}
		AVLTreeNode beta = node.left;
		AVLTreeNode t2 = beta.right;

		beta.right = node;
		node.left = t2;

		updatedHeight(node);
		updatedHeight(beta);

		return beta;
	}

	AVLTreeNode rotateLeft(AVLTreeNode node)
	{
		if (node == null) {
			return node;
		}

		AVLTreeNode beta = node.right;
		AVLTreeNode t2 = beta.left;
		beta.left=node;
		node.right = t2;

		updatedHeight(node);
		updatedHeight(beta);

		return beta;
	}

	int getBalance(AVLTreeNode node)
	{
		if (node == null) {
			return node;
		}
		int balance;

		balance = getHeight(node.left) - getHeight(node.right);
		return balance;
	}

	int getMinimum(AVLTreeNode node)
	{
		if (node == null) {
			return Integer.MIN_VALUE;
		}
		if (node.left == null) {
			return node.data;
		}
		return getMinimum(node);
	}

	AVLTreeNode insert(AVLTreeNode node,int key)
	{
		if (node == null) {
			return new AVLTreeNode(key);
		}
		if (key < node.data) {
			return insert(node.left,key);
		}
		else if (key > node.data) {
			return insert(node.right,key);
		}
		else {
			return node;
		}

		updatedHeight(node);
		int balance = getBalance(node);


		// right right or left right
		if (balance > 1) {
			if (key < node.left.data) {
				node = rotateRight(node);
			}
			else {
				node.left = rotateLeft(node.left);
				node = rotateRight(node);
			}
		}
		else if (balance < -1) {
			
			if (key > node.right.data) {
				node = rotateLeft(node);
			}
			else{
				node.right = rotateRight(node.right);
				node = rotateLeft(node);
			}
		}
		return node;
	}
}