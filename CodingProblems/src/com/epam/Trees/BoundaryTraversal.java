package com.epam.Trees;

public class BoundaryTraversal {
	
	public void printLeaves(Tree root)
	{
		if (root!=null) {
			  printLeaves(root.left);
				if (root.left == null && root.right == null) {
					System.out.print(root.data);
				}
			  printLeaves(root.right);	
		}
	}
	
	public void printLeft(Tree root) 
	{
		if (root!=null) 
		{
			if (root.left!=null) {
				System.out.print(root.data);
				printLeft(root.left);
			}
			else if (root.right!=null) {
				System.out.print(root.data);
				printLeft(root.right);
			}
		}
	}
	
	public void printRigh(Tree root) {
		if (root!=null) {
			if (root.right!=null) {
				printLeaves(root.right);
				System.out.print(root.data);
			}
			else if(root.left!=null)
			{
				printLeaves(root.left);
				System.out.print(root.data);
			}
		}
	}

	public static void main(String[] args) {
		

	}

}
