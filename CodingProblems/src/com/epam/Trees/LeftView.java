package com.epam.Trees;

public class LeftView {
	
	int max_level = 0;
	public void leftView(Tree root,int level)
	{
		if (root == null) {
			return;
		}
		if (max_level < level) {
			System.out.println(root.data);
			max_level = level;
		}
		
		leftView(root.left, level+1);
		leftView(root.right, level+1);
	}

}
