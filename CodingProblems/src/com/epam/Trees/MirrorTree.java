package com.epam.Trees;

public class MirrorTree {
	
	public Tree mirror(Tree root)
	{
		if (root == null) {
			return root;
		}
		Tree left = mirror(root.left);
		Tree right = mirror(root.right);
		
		root.left = right;
		root.right = left;
		return root;
		
	}

	public static void main(String[] args) {
		

	}

}
