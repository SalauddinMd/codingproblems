package com.epam.Trees;


public class MorrisTraversalInOrder {
	
	public void traversal(Tree root)
	{
		Tree current,pre;

		if (root == null) {
			return ;
		}
		current = root;
		while (current!=null) 
		{
			if (current.left == null) 
			{
					System.out.print(current.data + " ");
					current = current.right;
			}	

			else
			{
				pre = current.left;
				while(pre.right!=null && pre.right!=current)
				{
					pre = pre.right;
				}

				if (pre.right == null) 
				{
					pre.right = current;
					current = current.left;
				}
				else
				{
					pre.right = null;
					System.out.print(current.data+" ");
					current = current.right;
				}
			}
		}
	}

	public static void main(String[] args) 
	{
		Tree tree = new Tree(1);
        tree.left = new Tree(2);
        tree.right = new Tree(3);
        tree.left.left = new Tree(4);
        tree.left.right = new Tree(5);
        
        MorrisTraversalInOrder order = new MorrisTraversalInOrder();
        
        order.traversal(tree);
	}

}
