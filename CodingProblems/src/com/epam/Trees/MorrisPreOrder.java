package com.epam.Trees;

public class MorrisPreOrder {
	
	public void preOrder(Tree root)
	{
		while(root !=null) {
		
		if (root.left == null) 
		{
			System.out.print(root.data + " ");
			root = root.right;
		}
		else
		{
			Tree current = root.left;
			while (current.right != null && current.right!=root) 
			{
				current = current.right;
			}

			if (current.right == root) 
			{
				current.right = null;
				root = root.right;
			}
			else
			{
				System.out.print(root.data+" ");
				current.right = root;
				root = root.left;
			}
			}
		}
	}

	public static void main(String[] args) 
	{
		Tree tree = new Tree(1);
        tree.left = new Tree(2);
        tree.right = new Tree(3);
        tree.left.left = new Tree(4);
        tree.left.right = new Tree(5);
        
        MorrisPreOrder pre = new MorrisPreOrder();
        pre.preOrder(tree);

	}

}
