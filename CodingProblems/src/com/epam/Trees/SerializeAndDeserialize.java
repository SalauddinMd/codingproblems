package com.epam.Trees;

import java.util.ArrayList;

public class SerializeAndDeserialize {
	
	public void serialize(Tree root,ArrayList<Integer> array)
	{
		if (root == null) {
			array.add(-1);
			return;
		}
		array.add(root.data);
		serialize(root.left, array);
		serialize(root.right, array);
	}
	int index=0;
	public Tree deserialize(int[] array)
	{
		if (index >= array.length || array[index]== -1) {
			index+=1;
			return null;
		}
		Tree root = new Tree(array[index++]);
		root.left = deserialize(array);
		root.right = deserialize(array);
		return root;
	}

	public static void main(String[] args) {
		

	}

}
