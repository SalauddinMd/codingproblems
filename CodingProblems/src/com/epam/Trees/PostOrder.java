package com.epam.Trees;

import java.util.Stack;

public class PostOrder {
	
	public void OneStackPostOrder(Tree root)
	{
		if (root == null) {
			return;
		}
		
		Stack<Tree> stack = new Stack<>();
		Tree current = root;
		while(!stack.isEmpty() || current!=null)
		{
			if (current!=null) {
				stack.push(current);
				current=current.left;
			}
			else {
				Tree temp = stack.peek().right;
				if (temp == null) {
					temp = stack.pop();
					System.out.print(temp.data+" ");
					while(!stack.isEmpty() && temp == stack.peek().right)
					{
						temp = stack.pop();
						System.out.print(temp.data+" ");
					}
				}
				else {
					current = temp;
				}
			}
		}
	}

	public static void main(String[] args) {
		
		 	Tree tree = new Tree(1);
	        tree.left = new Tree(2);
	        tree.right = new Tree(3);
	        tree.left.left = new Tree(4);
	        tree.left.right = new Tree(5);
	        tree.right.left = new Tree(6);
	        tree.right.right = new Tree(7);
	        
	      PostOrder postOrder = new PostOrder();
	      postOrder.OneStackPostOrder(tree);
	  
	}

}
